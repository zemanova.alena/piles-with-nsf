# Piles with NSF - supplementary code for results presented

This repository provides support for the article "Settlement of Piles with Negative Skin Friction: Design Approaches Following New Eurocode 7". All source codes was written in Python 3 programming language and were created using Google Colaboratory Colab notebooks (hosted Jupyter Notebook service).


**Content of repository**

Example1.ipynb
Example2.ipynb


**Instructions**

The two supplemented files corespond to the examples introduced in the study, allow to run notebooks on the cloud for free without any prior installation, and provide figures presented in the article.

Futher instructions can be found on https://colab.research.google.com/

